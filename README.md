# bin-code-editor

基本的代码编辑器插件，现只支持json，后期扩展配置

## Docs

[document](https://wangbin3162.gitee.io/bin-code-editor/)

## Start

Clone or download this repository
Enter your local directory, and install dependencies:

```bash
yarn
```

## Develop

```bash
# serve with hot reload at localhost:8080
npm run dev
```

## Build

```bash
# build for production with minification
npm run build
```
